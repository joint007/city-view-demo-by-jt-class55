//google img
//all img/data are downloaded from server to your local laptop

//through protocolsL: such as http
//http is a protocol for fetching resources such as HTML documents.
//https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview

// how? API->Application Program Interface
//1. XMLHttp Request API(core of AJAX: Asynchronous JS and XML <text with style>)
//2. Fetch API (ES2015~): returns a promise (response  to the request)
//3. Axios: promise based http client, which uses XMLHttpRequest internally

fetch('https://jsonplaceholder.typicode.com/users')
.then(response => response.json())
    //the jason() method of the response interface takes a Response stream and reads it to completion
    //It returns a promise which resolves with the result of parsing the body text as JSON
.then(data => console.log(data))
.catch(err => console.log(err))
.finally(()=>console.log('game over'))

console.log('this line is running')
//async, sync
//synchronous
// |-----A-----|
//             |-----B-----|
//asynchronous
//|-----A-----|
//      |-----B-----|/

//Promise
//A promise has the following states:
//1. pending: initial state, neither fulfilled nor rejected
//2. fulfilled: meaning that the operation was completed successfully
//3. rejected: meaning that the operation failed
//4. settled
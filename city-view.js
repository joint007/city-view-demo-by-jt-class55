const accessKey = '8REe62qCVZNn9-9xjsmWAM7R0vp1hJAswbv3_zobJ1E'
const baseURL = 'https://api.unsplash.com/'
const eleInput = document.querySelector('.inputCity')
const eleSearch = document.querySelector('.fa-search')
const user_input = 'user_input'
let input_result = ''
let input_all_results = []


const searchFun = () => {
    eleInput.select() //select all texts
    input_result = eleInput.value.trim().toLowerCase()
    // console.log('key word====>',input_result)
    input_all_results.push(input_result)

    localStorage.setItem(user_input, JSON.stringify(input_result))
    localStorage.setItem('user_input_all_history', JSON.stringify(input_all_results))
    fetchCity()
}

const inputSearch = (evt) => {
    // console.log(evt.key)
    evt.key === 'Enter' && searchFun()
}

const fetchCity = () => {
    input_result = JSON.parse(localStorage.getItem(user_input))
    console.log(input_result)
    // if (input_result === null) {
    //     input_result = 'Toronto'
    // }
    // if (!input_result) {
    //     input_result = 'Toronto'
    // }
    !input_result && (input_result = 'Toronto')

    fetch(`${baseURL}search/photos?client_id=${accessKey}&query=${input_result}&orientation=landscape`)
        .then(response => response.json())
        .then(data => {
                // console.log('data=====>', data)
                // console.log(data.results[0].urls.regular)
                let result = data.results[0].urls.regular
                let des = data.results[0].alt_description
                let eleBody = document.querySelector('body')
                eleBody.style.backgroundImage = `url('${result}')`
                let eleDes = document.querySelector('.caption')
                eleDes.innerHTML = des
            }
        )
}

eleSearch.addEventListener('click', searchFun)
eleInput.addEventListener('keydown', inputSearch)

//when input get focus, all texts will be selected
eleInput.addEventListener('focusin', ()=>{
    eleInput.select()
})
fetchCity()





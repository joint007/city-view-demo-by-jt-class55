//variables
const starsDiv = document.getElementById("stars");
const starNum = 4;
//empty arr to save stars
const starsArr = [];


//function create initial starts
function initialStars(starNum){
    for(let i = 0;i < starNum;i++){
        const star = document.createElement("i");
        star.className = "fas fa-star";

        //add star into array
        starsArr.push(star);
        //put star onto html
        starsDiv.appendChild(star)
    }
}

initialStars(starNum);